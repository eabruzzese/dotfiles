# dotfiles

My dotfiles are managed with a small Ansible playbook. To install them on a macOS-based
machine, simply:

```shell
$ bin/setup
```

It may prompt for your sudo password so that it can apply OS-level settings.
